<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index (){

        $title = 'Willkommen';
        //return view('pages.index', compact('title'));//
        return view('pages.index')->with('title',$title);
    }

    public function about (){
        $title = 'Impressum';
        return view('pages.about')->with('title',$title);
    }

    public function services (){
        $data = array(
            'title' => 'Services',
            'services'=> ['Seminaranmeldung','Abmeldung', 'Widerruf' ]
        );

        return view('pages.services')->with($data);
    }
}
