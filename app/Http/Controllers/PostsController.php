<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use App\Post;
use DB;
use Input;
use Validator;
use Redirect;
class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except'=> ['index','show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //return = Post::all();
        //return = Post::where('title','Post Two')->get ();
        //$posts = DB::select('Select * from posts');
        //$posts = Post::orderby('title','asc')->take(1)->get();
        //$posts = Post::orderby('title','asc')->get();
        $posts = Post::orderby('created_at','desc')->paginate(11);
        return view('posts.index')->with('posts',$posts);
    }

    public function uploadfile(){
        return view ('posts.uploadfile');
    }


    public function insertfile(request $request){
        $validator = Validator::make($request->all(), [
            'Datei'   => 'mimes:pdf'
        ]);
if($request->hasFile('Datei')){
    $Dateiname = $request ->Datei->getClientOriginalName();
    $Dateigroesse = $request ->Datei->getClientSize();

    $request->Datei->storeAs('public/upload',$Dateiname);
    $file = new File;

    $file->Name = $Dateiname;

    $file->Größe = $Dateigroesse;

    $file->save();

    return redirect('/uploadfile')->with('uploaderfolg','Eintrag erfolgreich');
}
    return $request->all();

}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this -> validate($request, [
            'title'=> 'required ',
             'body'=> 'required']);
        //Posten

        $post = new Post;
        $post->title =$request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->save();

        return redirect('/posts')->with('success','Eintrag erfolgreich');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view ('posts.show')-> with ('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        //Check for correct User

        if(auth()->user()->id !==$post->user_id){
            return redirect('/posts')->with('error','unberechtigter Zugriff');

        }
        return view ('posts.edit')-> with ('post', $post);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this -> validate($request, [
            'title'=> 'required ',
            'body'=> 'required']);
        //Posten

        $post = Post::find($id);


        //Check for correct User

        if(auth()->user()->id !==$post->user_id){
            return redirect('/posts')->with('error','unberechtigter Zugriff');

        }
        $post->title =$request->input('title');
        $post->body = $request->input('body');
        $post->save();

        return redirect('/posts')->with('success','Bearbeitung erfolgreich');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $post = Post::find($id);

        //Check for correct User

        if(auth()->user()->id !==$post->user_id){
            return redirect('/posts')->with('error','unberechtigter Zugriff');

        }
       $post->delete();
       return redirect('/posts')->with('lösch','Eintrag Erfolgreich gelöscht');

    }
}
