<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //Tabellenname

    protected $table ='posts';

    //Primary Key

    public  $primaryKey ='ID';

    //Timestamps

    public $timestamps = true;

    public function user(){

        return $this -> belongsTo('App\User');
    }
}


