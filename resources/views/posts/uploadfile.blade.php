
@extends ('layouts.app')

@section('content')

<div class="container">
    <h3>Hisquis-Auszug Upload</h3>

    {!! Form::open(array('url'=>'insertfile','method'=>'POST', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data' ,'files' => true)) !!}


        <div class="form-group">
            <label class="control-label col-sm-2" for="pwd">Hochladen:</label>
            <div class="col-sm-10">
                <input type = "file" name="Datei" class="Datei">
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Hochladen</button>
            </div>
        </div>
    {!! Form::close()!!}
</div>

@endsection