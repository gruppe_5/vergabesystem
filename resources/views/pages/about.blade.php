@extends('layouts.app')

@section('content')
    <div class="container-fluid bg-grey">
        <h2 class="text-center">KONTAKT</h2>
        <div class="row">
            <div class="col-sm-5">
                <p>Das Seminarvergabesystem wurde von Gruppe 5 erstellt im Auftrag des Lehrstuhls für Wirtschaftinformatik . Bei evtl. auftretenden Frgagen kontaktieren sie uns unter folgendem Kontaktformular</p>
                <p><span class="glyphicon glyphicon-map-marker"></span> Passau, Bayern</p>
                <p><span class="glyphicon glyphicon-phone"></span> 01234/56789</p>
                <p><span class="glyphicon glyphicon-envelope"></span> Seminarvergabesystem@gmail.com</p>
            </div>
            <div class="col-sm-7">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="email" name="email" placeholder="E-Mail" type="email" required>
                    </div>
                </div>
                <textarea class="form-control" id="comments" name="comments" placeholder="Kommentar" rows="5"></textarea><br>
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <button class="btn btn-default pull-right" type="submit">Senden</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection