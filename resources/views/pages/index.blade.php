@extends('layouts.app')

@section('content')


    <div class="container-fluid text-center">
        <div class="row content">
            <div class="col-sm-2 sidenav">
                <br>
                <p><a href="https://www.uni-passau.de/"><h4>Uni Passau</h4></a></p>
                <p><a href="https://studip.uni-passau.de"><h4>Stud.IP</h4></a></p>
                <p><a href="https://qisserver.uni-passau.de/"><h4>HIS Online-Portal</h4></a></p>


            </div>
            <div class="col-sm-8 text-left">
                <h1>Willkommen!</h1>
                <p>Sehr geehrter Besucher, dies ist das Portal zur Seminarvergabe für Studenten der Uni Passau. Über den Reiter Hilfe bekommen sie mehr Informationen über den Ablauf für die jeweiligen Nutzergruppen. Unter dem Reiter Kontakt können sie sich bei Fragen direkt an uns wenden. Viel Erfolg!
                </p>

            </div>

        </div>
    </div>

    <footer class="container-fluid text-center">
        <p>Seminarvergabesystem Gruppe 5</p>
    </footer>

@endsection