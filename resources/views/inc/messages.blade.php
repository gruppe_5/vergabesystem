@if(count($errors)>0)
    @foreach($errors->all() as $error)
        <div class="alert alert-danger">
            <p>Leider ist ein Fehler aufgetreten:</p>
        </div>
    @endforeach
@endif

@if(session('success'))
    <div class="alert alert-success">

        <p>Eintrag erfolgreich</p>
    </div>
@endif

@if(session('lösch'))
    <div class="alert alert-success">

        <p>Eintrag erfolgreich gelöscht</p>
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">

        <p>Leider ist ein Fehler aufgetreten:</p>
    </div>
@endif

@if(session('uploaderfolg'))
    <div class="alert alert-success">

        <p>Hochladen erfolgreich:</p>
    </div>
@endif

@if(session('uploadfehl'))
    <div class="alert alert-danger">

        <p>Hochladen nicht erfolgreich: prüfen sie ob die Datei das PDF-Format besitzt</p>
    </div>
@endif